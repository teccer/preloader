package {

import flash.display.Loader;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.net.URLRequest;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

[SWF(width=800, height=600, frameRate=24)]

public class Preloader extends Sprite {
    private var _loader:Loader = new Loader(),
            _percentTxt:TextField = new TextField();

    public function Preloader() {
        _percentTxt.defaultTextFormat = new TextFormat(null, 48);
        _percentTxt.autoSize = TextFieldAutoSize.CENTER;
        _percentTxt.text = '0%';
        addChild(_percentTxt);
        _percentTxt.x = 300;
        _percentTxt.y = 200;

        _loader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgress);
        _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaded);
        _loader.load(new URLRequest("Main.swf"));
    }

    private function onProgress(e:ProgressEvent):void {
        var percent:Number = Math.round(e.bytesLoaded / e.bytesTotal * 100);
        _percentTxt.text = percent + '%';
    }

    private function onLoaded(e:Event):void {
        _loader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, onProgress);
        removeChild(_percentTxt);
        addChild(_loader.content);
    }
}
}
